﻿/* Adapted from https://github.com/MonoGame/MonoGame/blob/6fd168e26867d80a01203ed873ba4559d33eeb2a/MonoGame.Framework/Vector2.cs */

// MIT License - Copyright (C) The Mono.Xna Team
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.

using System;

namespace Abacus
{
    /// <summary>
    /// An immutable two dimensional point with real components.
    /// </summary>
    public struct Vector2 : IEquatable<Vector2>
    {
        #region Static Properties

        public static Vector2 Zero { get; } = new Vector2(0.00, 0.00);

        public static Vector2 UnitX { get; } = new Vector2(1.00, 0.00);

        public static Vector2 UnitY { get; } = new Vector2(0.00, 1.00);

        public static Vector2 One { get; } = new Vector2(1.00, 1.00);

        #endregion Properties

        #region Static Methods

        /// <summary>
        /// Performs vector addition on <paramref name="a"/> and
        /// <paramref name="b"/>, storing the result of the
        /// addition in <paramref name="o"/>.
        /// </summary>
        /// <param name="a">The first vector to add.</param>
        /// <param name="b">The second vector to add.</param>
        /// <param name="o">The result of the vector addition.</param>
        public static void Add(ref Vector2 a, ref Vector2 b, out Vector2 o) => o = new Vector2(a.X + b.X, a.Y + b.Y);

        /// <summary>
        /// Clamps the specified value within a range.
        /// </summary>
        /// <param name="a">The value to clamp.</param>
        /// <param name="min">The min value.</param>
        /// <param name="max">The max value.</param>
        /// <param name="o">The clamped value as an output parameter.</param>
        public static void Clamp(ref Vector2 a, ref Vector2 min, ref Vector2 max, out Vector2 o)
        {
            o = new Vector2(MathHelper.Clamp(a.X, min.X, max.X), MathHelper.Clamp(a.Y, min.Y, max.Y));
        }

        /// <summary>
        /// Returns the distance between two vectors.
        /// </summary>
        /// <param name="a">The first vector.</param>
        /// <param name="b">The second vector.</param>
        /// <param name="o">The distance between two vectors as an output parameter.</param>
        public static void Distance(ref Vector2 a, ref Vector2 b, out double o)
        {
            var dx = a.X - b.X;
            var dy = a.Y - b.Y;
            o = Math.Sqrt(dx * dx + dy * dy);
        }

        /// <summary>
        /// Returns the squared distance between two vectors.
        /// </summary>
        /// <param name="a">The first vector.</param>
        /// <param name="b">The second vector.</param>
        /// <param name="o">The squared distance between two vectors as an output parameter.</param>
        public static void DistanceSquared(ref Vector2 a, ref Vector2 b, out double o)
        {
            var dx = a.X - b.X;
            var dy = a.Y - b.Y;
            o = dx * dx + dy * dy;
        }

        /// <summary>
        /// Divides the components of a <see cref="Vector2"/> by the components of another <see cref="Vector2"/>.
        /// </summary>
        /// <param name="a">Source <see cref="Vector2"/>.</param>
        /// <param name="b">Divisor <see cref="Vector2"/>.</param>
        /// <param name="o">The result of dividing the vectors as an output parameter.</param>
        public static void Divide(ref Vector2 a, ref Vector2 b, out Vector2 o) => o = new Vector2(a.X / b.X, a.Y / b.Y);

        /// <summary>
        /// Divides the components of a <see cref="Vector2"/> by a scalar.
        /// </summary>
        /// <param name="a">Source <see cref="Vector2"/>.</param>
        /// <param name="b">Divisor scalar.</param>
        /// <param name="o">The result of dividing a vector by a scalar as an output parameter.</param>
        public static void Divide(ref Vector2 a, double b, out Vector2 o)
        {
            var factor = 1 / b;
            o = new Vector2(factor * a.X, factor * a.Y);
        }

        /// <summary>
        /// Creates a new <see cref="Vector2"/> that contains a multiplication of two vectors.
        /// </summary>
        /// <param name="a">Source <see cref="Vector2"/>.</param>
        /// <param name="b">Source <see cref="Vector2"/>.</param>
        /// <param name="o">The result of the vector multiplication as an output parameter.</param>
        public static void Multiply(ref Vector2 a, ref Vector2 b, out Vector2 o) => o = new Vector2(a.X * b.X, a.Y * b.Y);

        /// <summary>
        /// Creates a new <see cref="Vector2"/> that contains a multiplication of <see cref="Vector2"/> and a scalar.
        /// </summary>
        /// <param name="a">Source <see cref="Vector2"/>.</param>
        /// <param name="b">Scalar value.</param>
        /// <param name="o">The result of the multiplication with a scalar as an output parameter.</param>
        public static void Multiply(ref Vector2 a, double b, out Vector2 o) => o = new Vector2(a.X * b, a.Y * b);

        /// <summary>
        /// Creates a new <see cref="Vector2"/> that contains the specified vector inversion.
        /// </summary>
        /// <param name="a">Source <see cref="Vector2"/>.</param>
        /// <param name="o">The result of the vector inversion as an output parameter.</param>
        public static void Negate(ref Vector2 a, out Vector2 o) => o = new Vector2(-a.X, -a.Y);

        /// <summary>
        /// Creates a new <see cref="Vector2"/> that contains subtraction of on <see cref="Vector2"/> from a another.
        /// </summary>
        /// <param name="a">Source <see cref="Vector2"/>.</param>
        /// <param name="b">Source <see cref="Vector2"/>.</param>
        /// <param name="o">The result of the vector subtraction as an output parameter.</param>
        public static void Subtract(ref Vector2 a, ref Vector2 b, out Vector2 o) => o = new Vector2(a.X - b.X, a.Y - b.Y);

        #endregion

        #region Operators

        /// <summary>
        /// Inverts values in the specified <see cref="Vector2"/>.
        /// </summary>
        /// <param name="v">Source <see cref="Vector2"/> on the right of the sub sign.</param>
        /// <returns>Result of the inversion.</returns>
        public static Vector2 operator -(Vector2 v) => new Vector2(-v.X, -v.Y);

        /// <summary>
        /// Adds two vectors.
        /// </summary>
        /// <param name="a">Source <see cref="Vector2"/> on the left of the add sign.</param>
        /// <param name="b">Source <see cref="Vector2"/> on the right of the add sign.</param>
        /// <returns>Sum of the vectors.</returns>
        public static Vector2 operator +(Vector2 a, Vector2 b) => new Vector2(a.X + b.X, a.Y + b.Y);

        /// <summary>
        /// Subtracts a <see cref="Vector2"/> from a <see cref="Vector2"/>.
        /// </summary>
        /// <param name="a">Source <see cref="Vector2"/> on the left of the sub sign.</param>
        /// <param name="b">Source <see cref="Vector2"/> on the right of the sub sign.</param>
        /// <returns>Result of the vector subtraction.</returns>
        public static Vector2 operator -(Vector2 a, Vector2 b) => new Vector2(a.X - b.X, a.Y - b.Y);

        /// <summary>
        /// Multiplies the components of two vectors by each other.
        /// </summary>
        /// <param name="a">Source <see cref="Vector2"/> on the left of the mul sign.</param>
        /// <param name="b">Source <see cref="Vector2"/> on the right of the mul sign.</param>
        /// <returns>Result of the vector multiplication.</returns>
        public static Vector2 operator *(Vector2 a, Vector2 b) => new Vector2(a.X * b.X, a.Y * b.Y);

        /// <summary>
        /// Multiplies the components of vector by a scalar.
        /// </summary>
        /// <param name="a">Source <see cref="Vector2"/> on the left of the mul sign.</param>
        /// <param name="b">Scalar value on the right of the mul sign.</param>
        /// <returns>Result of the vector multiplication with a scalar.</returns>
        public static Vector2 operator *(Vector2 a, double b) => new Vector2(a.X * b, a.Y * b);

        /// <summary>
        /// Multiplies the components of vector by a scalar.
        /// </summary>
        /// <param name="a">Scalar value on the left of the mul sign.</param>
        /// <param name="b">Source <see cref="Vector2"/> on the right of the mul sign.</param>
        /// <returns>Result of the vector multiplication with a scalar.</returns>
        public static Vector2 operator *(double a, Vector2 b) => new Vector2(a * b.X, a * b.Y);

        /// <summary>
        /// Divides the components of a <see cref="Vector2"/> by the components of another <see cref="Vector2"/>.
        /// </summary>
        /// <param name="a">Source <see cref="Vector2"/> on the left of the div sign.</param>
        /// <param name="b">Divisor <see cref="Vector2"/> on the right of the div sign.</param>
        /// <returns>The result of dividing the vectors.</returns>
        public static Vector2 operator /(Vector2 a, Vector2 b) => new Vector2(a.X / b.X, a.Y / b.Y);

        /// <summary>
        /// Divides the components of a <see cref="Vector2"/> by a scalar.
        /// </summary>
        /// <param name="a">Source <see cref="Vector2"/> on the left of the div sign.</param>
        /// <param name="b">Divisor scalar on the right of the div sign.</param>
        /// <returns>The result of dividing a vector by a scalar.</returns>
        public static Vector2 operator /(Vector2 a, double b)
        {
            var factor = 1 / b;
            return new Vector2(factor * a.X, factor * a.Y);
        }

        /// <summary>
        /// Compares whether two <see cref="Vector2"/> instances are equal.
        /// </summary>
        /// <param name="a"><see cref="Vector2"/> instance on the left of the equal sign.</param>
        /// <param name="b"><see cref="Vector2"/> instance on the right of the equal sign.</param>
        /// <returns><c>true</c> if the instances are equal; <c>false</c> otherwise.</returns>
        public static bool operator ==(Vector2 a, Vector2 b) => Math.Abs(a.X - b.X) < Double.Epsilon && Math.Abs(a.Y - b.Y) < Double.Epsilon;

        /// <summary>
        /// Compares whether two <see cref="Vector2"/> instances are not equal.
        /// </summary>
        /// <param name="a"><see cref="Vector2"/> instance on the left of the not equal sign.</param>
        /// <param name="b"><see cref="Vector2"/> instance on the right of the not equal sign.</param>
        /// <returns><c>true</c> if the instances are not equal; <c>false</c> otherwise.</returns>	
        public static bool operator !=(Vector2 a, Vector2 b) => Math.Abs(a.X - b.X) > Double.Epsilon || Math.Abs(a.Y - b.Y) > Double.Epsilon;

        public static implicit operator Vector2(Point a) => new Vector2(a.X, a.Y);

        #endregion Operators

        #region Constructors

        public Vector2(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public Vector2(double u)
        {
            this.X = u;
            this.Y = u;
        }

        #endregion

        #region Properties

        public double X { get; }

        public double Y { get; }

        #endregion

        #region Methods

        public bool Equals(Vector2 other)
        {
            return this.X.Equals(other.X) && this.Y.Equals(other.Y);
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other))
                return false;
            return other is Vector2 && this.Equals((Vector2)other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.X.GetHashCode() * 397) ^ this.Y.GetHashCode();
            }
        }

        /// <summary>
        /// Returns the length of this <see cref="Vector2"/>.
        /// </summary>
        /// <returns>The length of this <see cref="Vector2"/>.</returns>
        public double Length() => Math.Sqrt(this.X * this.X + this.Y * this.Y);

        /// <summary>
        /// Returns the squared length of this <see cref="Vector2"/>.
        /// </summary>
        /// <returns>The squared length of this <see cref="Vector2"/>.</returns>
        public double LengthSquared() => this.X * this.X + this.Y * this.Y;

        public Vector2 Normalize() => this * (1.00 / this.LengthSquared());

        public string ToString(string format) => $"{{X:{this.X.ToString(format)}; Y:{this.Y.ToString(format)}}}";

        public override string ToString() => $"{{X:{this.X}; Y:{this.Y}}}";

        #endregion

        #region Xna

#if XNA
        public static implicit operator Microsoft.Xna.Framework.Vector2(Vector2 a) => new Microsoft.Xna.Framework.Vector2((float)a.X, (float)a.Y);

        public static implicit operator Vector2(Microsoft.Xna.Framework.Vector2 a) => new Vector2(a.X, a.Y);
#endif

        #endregion
    }
}
