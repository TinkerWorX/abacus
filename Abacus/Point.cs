﻿/* Adapted from https://github.com/MonoGame/MonoGame/blob/6fd168e26867d80a01203ed873ba4559d33eeb2a/MonoGame.Framework/Vector2.cs */

// MIT License - Copyright (C) The Mono.Xna Team
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.

using System;

namespace Abacus
{
    /// <summary>
    /// An immutable two dimensional point with integer components.
    /// </summary>
    public struct Point : IEquatable<Point>
    {
        #region Static Properties

        public static Point Zero { get; } = new Point(0, 0);

        public static Point UnitX { get; } = new Point(1, 0);

        public static Point UnitY { get; } = new Point(0, 1);

        public static Point One { get; } = new Point(1, 1);

        #endregion Properties

        #region Static Methods

        /// <summary>
        /// Performs vector addition on <paramref name="a"/> and
        /// <paramref name="b"/>, storing the result of the
        /// addition in <paramref name="o"/>.
        /// </summary>
        /// <param name="a">The first vector to add.</param>
        /// <param name="b">The second vector to add.</param>
        /// <param name="o">The result of the vector addition.</param>
        public static void Add(ref Point a, ref Point b, out Point o) => o = new Point(a.X + b.X, a.Y + b.Y);

        /// <summary>
        /// Clamps the specified value within a range.
        /// </summary>
        /// <param name="a">The value to clamp.</param>
        /// <param name="min">The min value.</param>
        /// <param name="max">The max value.</param>
        /// <param name="o">The clamped value as an output parameter.</param>
        public static void Clamp(ref Point a, ref Point min, ref Point max, out Point o) => o = new Point(MathHelper.Clamp(a.X, min.X, max.X), MathHelper.Clamp(a.Y, min.Y, max.Y));

        /// <summary>
        /// Returns the distance between two vectors.
        /// </summary>
        /// <param name="a">The first vector.</param>
        /// <param name="b">The second vector.</param>
        /// <param name="o">The distance between two vectors as an output parameter.</param>
        public static void Distance(ref Point a, ref Point b, out double o)
        {
            var dx = a.X - b.X;
            var dy = a.Y - b.Y;
            o = Math.Sqrt(dx * dx + dy * dy);
        }

        /// <summary>
        /// Returns the squared distance between two vectors.
        /// </summary>
        /// <param name="a">The first vector.</param>
        /// <param name="b">The second vector.</param>
        /// <param name="o">The squared distance between two vectors as an output parameter.</param>
        public static void DistanceSquared(ref Point a, ref Point b, out double o)
        {
            var dx = a.X - b.X;
            var dy = a.Y - b.Y;
            o = dx * dx + dy * dy;
        }

        /// <summary>
        /// Divides the components of a <see cref="Point"/> by the components of another <see cref="Point"/>.
        /// </summary>
        /// <param name="a">Source <see cref="Point"/>.</param>
        /// <param name="b">Divisor <see cref="Point"/>.</param>
        /// <param name="o">The result of dividing the vectors as an output parameter.</param>
        public static void Divide(ref Point a, ref Point b, out Point o) => o = new Point(a.X / b.X, a.Y / b.Y);

        /// <summary>
        /// Divides the components of a <see cref="Point"/> by a scalar.
        /// </summary>
        /// <param name="a">Source <see cref="Point"/>.</param>
        /// <param name="b">Divisor scalar.</param>
        /// <param name="o">The result of dividing a vector by a scalar as an output parameter.</param>
        public static void Divide(ref Point a, int b, out Point o) => o = new Point(a.X / b, a.Y / b);

        /// <summary>
        /// Creates a new <see cref="Point"/> that contains a multiplication of two vectors.
        /// </summary>
        /// <param name="a">Source <see cref="Point"/>.</param>
        /// <param name="b">Source <see cref="Point"/>.</param>
        /// <param name="o">The result of the vector multiplication as an output parameter.</param>
        public static void Multiply(ref Point a, ref Point b, out Point o) => o = new Point(a.X * b.X, a.Y * b.Y);

        /// <summary>
        /// Creates a new <see cref="Point"/> that contains a multiplication of <see cref="Point"/> and a scalar.
        /// </summary>
        /// <param name="a">Source <see cref="Point"/>.</param>
        /// <param name="b">Scalar value.</param>
        /// <param name="o">The result of the multiplication with a scalar as an output parameter.</param>
        public static void Multiply(ref Point a, int b, out Point o) => o = new Point(a.X * b, a.Y * b);

        /// <summary>
        /// Creates a new <see cref="Point"/> that contains the specified vector inversion.
        /// </summary>
        /// <param name="a">Source <see cref="Point"/>.</param>
        /// <param name="o">The result of the vector inversion as an output parameter.</param>
        public static void Negate(ref Point a, out Point o) => o = new Point(-a.X, -a.Y);

        /// <summary>
        /// Creates a new <see cref="Point"/> that contains subtraction of on <see cref="Point"/> from a another.
        /// </summary>
        /// <param name="a">Source <see cref="Point"/>.</param>
        /// <param name="b">Source <see cref="Point"/>.</param>
        /// <param name="o">The result of the vector subtraction as an output parameter.</param>
        public static void Subtract(ref Point a, ref Point b, out Point o) => o = new Point(a.X - b.X, a.Y - b.Y);

        #endregion

        #region Operators

        /// <summary>
        /// Inverts values in the specified <see cref="Point"/>.
        /// </summary>
        /// <param name="v">Source <see cref="Point"/> on the right of the sub sign.</param>
        /// <returns>Result of the inversion.</returns>
        public static Point operator -(Point v) => new Point(-v.X, -v.Y);

        /// <summary>
        /// Adds two vectors.
        /// </summary>
        /// <param name="a">Source <see cref="Point"/> on the left of the add sign.</param>
        /// <param name="b">Source <see cref="Point"/> on the right of the add sign.</param>
        /// <returns>Sum of the vectors.</returns>
        public static Point operator +(Point a, Point b) => new Point(a.X + b.X, a.Y + b.Y);

        /// <summary>
        /// Subtracts a <see cref="Point"/> from a <see cref="Point"/>.
        /// </summary>
        /// <param name="a">Source <see cref="Point"/> on the left of the sub sign.</param>
        /// <param name="b">Source <see cref="Point"/> on the right of the sub sign.</param>
        /// <returns>Result of the vector subtraction.</returns>
        public static Point operator -(Point a, Point b) => new Point(a.X - b.X, a.Y - b.Y);

        /// <summary>
        /// Multiplies the components of two vectors by each other.
        /// </summary>
        /// <param name="a">Source <see cref="Point"/> on the left of the mul sign.</param>
        /// <param name="b">Source <see cref="Point"/> on the right of the mul sign.</param>
        /// <returns>Result of the vector multiplication.</returns>
        public static Point operator *(Point a, Point b) => new Point(a.X * b.X, a.Y * b.Y);

        /// <summary>
        /// Multiplies the components of vector by a scalar.
        /// </summary>
        /// <param name="a">Source <see cref="Point"/> on the left of the mul sign.</param>
        /// <param name="b">Scalar value on the right of the mul sign.</param>
        /// <returns>Result of the vector multiplication with a scalar.</returns>
        public static Point operator *(Point a, int b) => new Point(a.X * b, a.Y * b);

        /// <summary>
        /// Multiplies the components of vector by a scalar.
        /// </summary>
        /// <param name="a">Scalar value on the left of the mul sign.</param>
        /// <param name="b">Source <see cref="Point"/> on the right of the mul sign.</param>
        /// <returns>Result of the vector multiplication with a scalar.</returns>
        public static Point operator *(int a, Point b) => new Point(a * b.X, a * b.Y);

        /// <summary>
        /// Divides the components of a <see cref="Point"/> by the components of another <see cref="Point"/>.
        /// </summary>
        /// <param name="a">Source <see cref="Point"/> on the left of the div sign.</param>
        /// <param name="b">Divisor <see cref="Point"/> on the right of the div sign.</param>
        /// <returns>The result of dividing the vectors.</returns>
        public static Point operator /(Point a, Point b) => new Point(a.X / b.X, a.Y / b.Y);

        /// <summary>
        /// Divides the components of a <see cref="Point"/> by a scalar.
        /// </summary>
        /// <param name="a">Source <see cref="Point"/> on the left of the div sign.</param>
        /// <param name="b">Divisor scalar on the right of the div sign.</param>
        /// <returns>The result of dividing a vector by a scalar.</returns>
        public static Point operator /(Point a, int b) => new Point(a.X / b, a.Y / b);

        /// <summary>
        /// Compares whether two <see cref="Point"/> instances are equal.
        /// </summary>
        /// <param name="a"><see cref="Point"/> instance on the left of the equal sign.</param>
        /// <param name="b"><see cref="Point"/> instance on the right of the equal sign.</param>
        /// <returns><c>true</c> if the instances are equal; <c>false</c> otherwise.</returns>
        public static bool operator ==(Point a, Point b) => a.X == b.X && a.Y == b.Y;

        /// <summary>
        /// Compares whether two <see cref="Point"/> instances are not equal.
        /// </summary>
        /// <param name="a"><see cref="Point"/> instance on the left of the not equal sign.</param>
        /// <param name="b"><see cref="Point"/> instance on the right of the not equal sign.</param>
        /// <returns><c>true</c> if the instances are not equal; <c>false</c> otherwise.</returns>	
        public static bool operator !=(Point a, Point b) => a.X != b.X || a.Y != b.Y;

        /// <remarks>Explicit because there is a potential loss of data.</remarks>
        public static explicit operator Point(Vector2 a) => new Point((int)a.X, (int)a.Y);

        #endregion Operators

        #region Constructors

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public Point(int u)
        {
            this.X = u;
            this.Y = u;
        }

        #endregion

        #region Properties

        public int X { get; }

        public int Y { get; }

        #endregion

        #region Methods

        public bool Equals(Point other)
        {
            return this.X.Equals(other.X) && this.Y.Equals(other.Y);
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other))
                return false;
            return other is Point && this.Equals((Point)other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.X.GetHashCode() * 397) ^ this.Y.GetHashCode();
            }
        }

        /// <summary>
        /// Returns the length of this <see cref="Point"/>.
        /// </summary>
        /// <returns>The length of this <see cref="Point"/>.</returns>
        public double Length() => Math.Sqrt(this.X * this.X + this.Y * this.Y);

        /// <summary>
        /// Returns the squared length of this <see cref="Point"/>.
        /// </summary>
        /// <returns>The squared length of this <see cref="Point"/>.</returns>
        public int LengthSquared() => this.X * this.X + this.Y * this.Y;

        public string ToString(string format) => $"{{X:{this.X.ToString(format)}; Y:{this.Y.ToString(format)}}}";

        public override string ToString() => $"{{X:{this.X}; Y:{this.Y}}}";

        #endregion

        #region Xna

#if XNA
        public static implicit operator Microsoft.Xna.Framework.Point(Point a) => new Microsoft.Xna.Framework.Point(a.X, a.Y);

        public static implicit operator Point(Microsoft.Xna.Framework.Point a) => new Point(a.X, a.Y);
#endif

        #endregion
    }
}
